#[derive(RustcDecodable)]
pub struct DrillData {
    pub date: String,
    pub time: String,
    pub depth: f32,
    pub vibration: f32,
    pub long: f64,
    pub lat: f64,
}

#[derive(RustcDecodable)]
pub struct SurveyorData {
    pub lat: f64,
    pub long: f64,
}
