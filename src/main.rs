extern crate haversine;
extern crate csv;
extern crate rustc_serialize;

pub mod structs;

use structs::DrillData;
use structs::SurveyorData;

fn main() {
    let mut drill_reader = csv::Reader::from_file("/home/vignesh/Documents/2017-05-invoice.csv").unwrap();
    let mut surveyor_reader = csv::Reader::from_file("/home/vignesh/Documents/surveyor-data.csv").unwrap().has_headers(false);
    let mut records = Vec::new();
    let mut surveyor_data = Vec::new();

    for row in drill_reader.decode() {
        let record: DrillData = row.unwrap();
        records.push(record);
    }

    for row in surveyor_reader.decode() {
        let record: SurveyorData = row.unwrap();
        surveyor_data.push(record);
    }

    println!("Distances");
    for i in 0..7 {
        let start = haversine::Location {
            latitude: records[i].lat,
            longitude: records[i].long,
        };

        let finish = haversine::Location {
            latitude: surveyor_data[i].lat,
            longitude: records[i].long,
        };

        let distance = haversine::distance(start, finish, haversine::Units::Kilometers);
        println!("{}", distance * 1000.00);
    }
}